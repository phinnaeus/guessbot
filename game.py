import collections
from collections.abc import Hashable

from typing import List, Dict


class Guess(Hashable):
    def __init__(self, author_id: int, message_id: int, guessed_number: float):
        self.author_id: int = author_id
        self.message_id: int = message_id
        self.guessed_number: float = guessed_number

    def __hash__(self) -> int:
        return hash(self.author_id) + hash(self.message_id) + hash(self.guessed_number)

    def __eq__(self, other) -> bool:
        if not isinstance(other, Guess):
            return False
        return (
            self.author_id == other.author_id
            and self.message_id == other.message_id
            and self.guessed_number == other.guessed_number
        )

    def __str__(self):
        return f"<@{self.author_id}> ({self.guessed_number})"


class Game(Hashable):
    def __init__(self, channel_id: int, name: str, per_user_limit=0):
        self.channel_id: int = channel_id
        self.name: str = name
        self.done: bool = False
        # TODO change this if we support negative answers
        self.correct_number: float = -1
        self.guesses: Dict[float, Guess] = {}
        self.per_user_limit = per_user_limit
        self.per_user_count: Dict[int, int] = collections.defaultdict(lambda: 0)

    def add_guess(self, guess: Guess) -> None:
        if self.done:
            raise AlreadyDoneException()
        if guess.guessed_number in self.guesses:
            raise AlreadyGuessedException(self.guesses[guess.guessed_number])
        guess_count = self.per_user_count[guess.author_id]
        previous_guess = None
        if guess_count > 0:
            for existing in self.guesses.values():
                if existing.author_id == guess.author_id:
                    previous_guess = existing.guessed_number
                    break
            # sorry about this, but my IDE offered to "simplify" it
            # basically, only check the guess count against the per-user limit if the limit is > 0
            if 0 < self.per_user_limit == guess_count:
                raise TooManyGuessesException()

        self.guesses[guess.guessed_number] = guess
        self.per_user_count[guess.author_id] += 1

        # do this after adding the new guess to avoid a race condition where this
        # person can't add a new guess
        # note that there's probably already a race condition or four here
        if previous_guess:
            del self.guesses[previous_guess]

    def complete_game(self, correct_number: float) -> List[Guess]:
        self.done = True
        self.correct_number = correct_number
        if len(self.guesses) == 0:
            raise NoGuessesException()

        if correct_number in self.guesses:
            # found an exact match
            return [self.guesses[correct_number]]

        sorted_guesses = sorted(list(self.guesses.keys()) + [correct_number])
        correct_index = sorted_guesses.index(correct_number)
        if correct_index == 0:
            return [self.guesses[sorted_guesses[1]]]
        if correct_index == (len(sorted_guesses) - 1):
            return [self.guesses[sorted_guesses[-2]]]

        left_dist = abs(sorted_guesses[correct_index - 1] - correct_number)
        right_dist = abs(sorted_guesses[correct_index + 1] - correct_number)
        if left_dist < right_dist:
            return [self.guesses[sorted_guesses[correct_index - 1]]]
        elif right_dist < left_dist:
            return [self.guesses[sorted_guesses[correct_index + 1]]]
        else:
            # TIE!
            return [
                self.guesses[sorted_guesses[correct_index - 1]],
                self.guesses[sorted_guesses[correct_index + 1]],
            ]

    def __eq__(self, other) -> bool:
        if not isinstance(other, Game):
            return False
        return self.channel_id == other.channel_id and self.name == other.name

    def __hash__(self) -> int:
        return hash(self.channel_id) + hash(self.name)


class HistoricalGame:
    def __init__(self, completed_game: Game):
        if not completed_game.done:
            raise NotYetDoneException
        self.name: str = completed_game.name
        self.channel_id: int = completed_game.channel_id
        self.correct_number: float = completed_game.correct_number
        self.guesses: Dict[float, int] = {
            g.guessed_number: g.author_id for g in completed_game.guesses.values()
        }


class AlreadyGuessedException(Exception):
    def __init__(self, previous_guess: Guess):
        super()
        self.previous_guess = previous_guess


class AlreadyDoneException(Exception):
    pass


class NotYetDoneException(Exception):
    pass


class NoGuessesException(Exception):
    pass


class TooManyGuessesException(Exception):
    pass
