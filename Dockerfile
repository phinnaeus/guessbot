# This Dockerfile uses multi-stage build to customize DEV and PROD images:
# https://docs.docker.com/develop/develop-images/multistage-build/

# shamelessly stolen from wemake
# https://github.com/wemake-services/wemake-django-template/tree/master/%7B%7Bcookiecutter.project_name%7D%7D/docker/django

FROM python:3.10.2-slim-bullseye AS poetry_base

ENV PYTHONFAULTHANDLER=1 \
  PYTHONHASHSEED=random \
  PYTHONDONTWRITEBYTECODE=1 \
  # pip:
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100 \
  # poetry:
  POETRY_VERSION=1.1.13 \
  POETRY_NO_INTERACTION=1

RUN apt-get update && apt-get upgrade -y \
  && apt-get install --no-install-recommends -y \
    curl \
    git \
  # Cleaning cache:
  && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false \
  && apt-get clean -y \
  && rm -rf /var/lib/apt/lists/* \
  # create a user
  && groupadd -r web \
  && mkdir /code \
  && useradd -d /code -r -g web web \
  && chown web:web -R /code \
  && mkdir /persist \
  && chown web:web -R /persist

# Running as non-root user:
USER web
ENV PATH="$PATH:/code/.local/bin"

# install poetry
RUN curl -sSL https://install.python-poetry.org | python - \
  && poetry --version

FROM poetry_base AS development_build

WORKDIR /code

# Copy only requirements, to cache them in docker layer
COPY --chown=web:web ./poetry.lock ./pyproject.toml /code/

# Project initialization:
RUN poetry version \
  && poetry install --no-dev --no-interaction --no-ansi \
  # Cleaning poetry installation's cache for production:
  && rm -rf "$POETRY_CACHE_DIR"

# We customize how our app is loaded with the custom entrypoint:
CMD [ "poetry", "run", "guessbot" ]

# The following stage is only for Prod:
# https://wemake-django-template.readthedocs.io/en/latest/pages/template/production.html
FROM development_build AS production_build
COPY --chown=web:web . /code