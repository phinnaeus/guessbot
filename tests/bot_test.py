from bot import Bot
from unittest.mock import MagicMock
import pytest


@pytest.mark.asyncio
async def test_avoid_self_messages():
    bot = Bot()
    channel = MagicMock()
    msg = MagicMock()
    # msg.author = 'test'
    msg.channel = channel
    await bot.on_message(MagicMock(), msg)
    channel.call_count
