from game import Game, Guess
from pytest import raises


def test_game_add_guess():
    game = Game(0, "test")
    assert len(game.guesses) == 0
    game.add_guess(Guess(0, 0, 10))
    assert len(game.guesses) == 1


def test_game_add_guess_float():
    game = Game(0, "test")
    assert len(game.guesses) == 0
    game.add_guess(Guess(0, 0, 0.0012421))
    assert len(game.guesses) == 1


def test_game_add_guess_complete():
    game = Game(0, "test")
    game.add_guess(Guess(0, 0, 10))
    game.complete_game(10)
    with raises(Exception):
        game.add_guess(Guess(0, 0, 10))


def test_game_add_guess_complete_float():
    game = Game(0, "test")
    game.add_guess(Guess(0, 0, -124.3251521351))
    game.complete_game(-10)
    with raises(Exception):
        game.add_guess(Guess(0, 0, -101021))


def test_game_add_guess_duplicate():
    game = Game(0, "test")
    game.add_guess(Guess(0, 0, 10))
    with raises(Exception):
        game.add_guess(Guess(1, 1, 10))
    assert len(game.guesses) == 1


def test_game_add_guess_duplicate_float():
    game = Game(0, "test")
    game.add_guess(Guess(0, 0, -10.0))
    with raises(Exception):
        game.add_guess(Guess(1, 1, -10.00))
    assert len(game.guesses) == 1


def test_game_add_guess_overwrite():
    game = Game(0, "test")
    game.add_guess(Guess(0, 0, 10))
    game.add_guess(Guess(0, 1, 11))
    assert len(game.guesses) == 1
    assert game.guesses[11].author_id == 0


def test_per_user_limit_unlimited():
    game = Game(0, "test", per_user_limit=0)
    game.add_guess(Guess(0, 0, 10))
    game.add_guess(Guess(0, 0, 11))
    game.add_guess(Guess(0, 0, 12))
    game.add_guess(Guess(0, 0, 13))
    assert len(game.guesses) == 1
    assert game.guesses[13].author_id == 0


def test_per_user_limit():
    game = Game(0, "test", per_user_limit=1)
    game.add_guess(Guess(0, 0, 10))
    with raises(Exception):
        game.add_guess(Guess(0, 1, 11))


def test_game_complete():
    game = Game(0, "test")
    game.add_guess(Guess(2, 3, 10))
    winners = game.complete_game(10)
    assert len(winners) == 1
    assert winners[0].guessed_number == 10
    assert winners[0].author_id == 2
    assert winners[0].message_id == 3


def test_game_complete_float():
    game = Game(0, "test")
    game.add_guess(Guess(2, 3, -10.0))
    winners = game.complete_game(-3.004)
    assert len(winners) == 1
    assert winners[0].guessed_number == -10.0
    assert winners[0].author_id == 2
    assert winners[0].message_id == 3


def test_game_complete_no_guesses():
    game = Game(0, "test")
    with raises(Exception):
        game.complete_game(10)


def test_game_complete_tie():
    game = Game(0, "test")
    game.add_guess(Guess(1, 2, 10))
    game.add_guess(Guess(2, 3, 12))
    winners = game.complete_game(11)
    assert len(winners) == 2


def test_game_complete_tie_float():
    game = Game(0, "test")
    game.add_guess(Guess(1, 2, 9.9))
    game.add_guess(Guess(2, 3, 12.1))
    winners = game.complete_game(11.0)
    assert len(winners) == 2


def test_game_complete_inexact():
    game = Game(0, "test")
    game.add_guess(Guess(1, 2, 10))
    game.add_guess(Guess(2, 3, 12))
    winners = game.complete_game(13)
    assert len(winners) == 1
    assert winners[0].guessed_number == 12
    assert winners[0].author_id == 2
    assert winners[0].message_id == 3
