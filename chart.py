import logging
from pathlib import Path
import tempfile
from game import Game
import matplotlib
from matplotlib import pyplot
import numpy as np

matplotlib.use("Agg")

logger = logging.getLogger("chart")


class Chart:
    def __init__(self, game: Game):
        if not game.done:
            raise Exception("game is not yet done")
        self.all_guesses = list(game.guesses.keys()) + [game.correct_number]
        self.correct_number = game.correct_number

    def box_plot(self) -> Path:
        x = np.array(self.all_guesses)
        fig = pyplot.figure(figsize=(5, 2))
        ax = fig.add_subplot()
        ax.set_xlim(0, int(1.15 * max(self.all_guesses)))
        ax.boxplot(x, showmeans=True, vert=False)  # type: ignore
        ax.annotate(  # type: ignore
            f"Actual ({self.correct_number})",
            xy=(self.correct_number, 1),
            xycoords="data",
            xytext=(0.5, 0.85),
            textcoords="axes fraction",
            arrowprops=dict(facecolor="black", headwidth=7, width=1),
            horizontalalignment="center",
            verticalalignment="top",
        )
        _, name = tempfile.mkstemp(suffix=".png")
        logger.info(f"created temporary file {name}")
        pyplot.savefig(name)
        return Path(name)
