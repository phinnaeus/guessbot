# GuessBot

GuessBot is a simple Discord bot which facilitates numeric guessing games.
The general idea is a game is started in a specific channel or thread, and
users type their guesses which the bot tracks until the game is finalized
at which point the real number is revealed and the closest guesser is
given a token trophy emoji.

The use case for this was COVID-19 case numbers, specifically the late 2021
outbreak in NSW, Australia.

## Future
Unfortunately this project can probably be considered dead as the discord.py
library it is written on top of has been broken by Discord. See
[here](https://gist.github.com/Rapptz/4a2f62751b9600a31a0d3c78100287f1) for
details. 
