import json
import logging
import pathlib
import time
import random
from statistics import median, mean
from typing import Dict, List, Optional

import discord

from chart import Chart
from game import (
    Game,
    Guess,
    HistoricalGame,
    AlreadyGuessedException,
    AlreadyDoneException,
    NoGuessesException,
    TooManyGuessesException,
)

BETA_USER_ID = 879168048014180452

MAX_GUESS_SIZE = 64

logger = logging.getLogger("bot")


class Bot:
    def __init__(self, games=None, history_path=None):
        if games is None:
            games = {}
        self.games: Dict[int, Game] = games
        self.history_path: pathlib.Path = history_path

    @staticmethod
    async def print_help(channel: discord.TextChannel):
        await channel.send(
            "Channel moderators can tell me to `start <game name> [guess limit]` or `complete <correct answer>`."
        )

    @staticmethod
    async def permissions_check(user: discord.Member, channel: discord.TextChannel) -> bool:
        if channel.permissions_for(user).manage_channels:
            return True
        await channel.send("You need to have manage_channel permissions to send me command.")
        return False

    async def start_cmd(self, channel: discord.TextChannel, game: Optional[Game], args: List[str]):
        if game:
            await channel.send(
                f"There is already a game in progress in this channel: {game.name}. End it first!"
            )
        else:
            # TODO should we just use argparse for this LOL
            if len(args) < 1:
                await channel.send(
                    "Please provide the name of the game to start with `start <game name> [guess limit]`."
                )
                return
            limit = 0
            if len(args) >= 2:
                limit = int(args[1])
            game = Game(channel.id, args[0], per_user_limit=limit)
            self.games[channel.id] = game
            await channel.send(
                f"Guessing game `{game.name}` started. To participate, simply send a message in "
                "this channel/thread with your guess. To update your guess, send a new message."
            )
            if limit:
                await channel.send(
                    f"*Important*: this game allows a maximum of {limit} guess(es) per user!"
                )

    async def complete_cmd(
        self, channel: discord.TextChannel, game: Optional[Game], args: List[str]
    ):
        if not game:
            await channel.send(
                "No guessing game has been started yet. Start one by sending me an @ that says "
                "`start <game name> [guess limit]`"
            )
        else:
            if len(args) < 1:
                await channel.send(
                    "Please provide the correct answer with `complete <correct number>`."
                )
                return
            try:
                correct_number = float(args[0])
            except ValueError:
                await channel.send("Please provide a number for the correct number.")
                return

            try:
                winners = game.complete_game(correct_number)
                response = f"`{game.name}` is complete! "
                distance = abs(winners[0].guessed_number - correct_number)
                if len(winners) > 1:
                    distance = abs(winners[0].guessed_number - correct_number)
                    response += f"Two way tie between {winners[0]} and {winners[1]}. Both were {distance} away."
                else:
                    response += f"{winners[0]} wins with a distance of {distance}."

                try:
                    chart_file = Chart(game).box_plot()
                except Exception as e:
                    chart_file = None
                    logger.error(f"Unable to create chart: {e}")
                await channel.send(response, file=discord.File(chart_file))

                for winner in winners:
                    try:
                        winning_msg = await channel.fetch_message(winner.message_id)
                        await winning_msg.add_reaction("🏆")
                    except discord.NotFound as e:
                        logger.error(f"Unable to find message {winner.message_id}: {e}")

            except NoGuessesException:
                await channel.send("No one guessed!")
            if self.history_path:
                self.history_path.mkdir(parents=True, exist_ok=True)
                record_path = self.history_path / time.strftime(
                    f"%Y%m%d-%H%M%S_{game.channel_id}-{game.name}"
                )
                history = HistoricalGame(game).__dict__
                with record_path.open("w") as f:
                    logger.info(f"saving completed game: {history}")
                    json.dump(history, f)
            del self.games[channel.id]

    @staticmethod
    async def list_cmd(channel: discord.TextChannel, game: Optional[Game], _: List[str]):
        if not game:
            await channel.send(
                "No guessing game has been started yet. Start one by sending me an @ that says "
                "`start <game name>`"
            )
        else:
            if len(game.guesses) == 0:
                await channel.send("No guesses have been made yet.")
                return
            sorted_guesses = sorted(game.guesses.keys())
            await channel.send(
                f"Current guesses: `{sorted_guesses}`\nMean: `{mean(sorted_guesses)}`\n"
                f"Median: `{median(sorted_guesses)}`"
            )

    @staticmethod
    async def freeze_cmd(channel: discord.TextChannel, game: Optional[Game], _: List[str]):
        if not game:
            await channel.send(
                "No guessing game has been started yet. Start one by sending me an @ that says "
                "`start <game name>`"
            )
        else:
            game.done = True
            await channel.send(
                f"`{game.name}` has been frozen, no more guesses will be accepted. The game can be "
                "finalized with `complete <correct answer>`"
            )

    async def handle_mention(
        self, msg: discord.Message, game: Optional[Game], beta_bot: bool = False
    ):
        tokens: List[str] = [t.strip() for t in msg.content.split(" ") if t]
        command = tokens[1]

        logger.debug(f"got command: {tokens} in {msg.channel.name}")

        if command == "help" or command == "?":
            await self.print_help(msg.channel)
        elif command == "start":
            if not await self.permissions_check(msg.author, msg.channel):
                return
            await self.start_cmd(msg.channel, game, tokens[2:])
        elif command == "show":
            await self.list_cmd(msg.channel, game, tokens[2:])
        elif command == "freeze":
            if not await self.permissions_check(msg.author, msg.channel):
                return
            await self.freeze_cmd(msg.channel, game, tokens[2:])
        elif command == "complete":
            if not await self.permissions_check(msg.author, msg.channel):
                return
            await self.complete_cmd(msg.channel, game, tokens[2:])
        elif command == "add_test_data" and beta_bot:
            if game:
                for i in range(25):
                    game.add_guess(Guess(i, i, random.randint(0, 1000)))

    @staticmethod
    async def handle_dm(msg: discord.Message):
        logging.debug(f"event: DM from {msg.author.name}")
        await msg.channel.send("Hi! Unfortunately I do not support private guesses yet :(")

    async def on_message(self, user: discord.User, msg: discord.Message):
        if msg.author == user:
            return

        if isinstance(msg.channel, discord.DMChannel):
            await self.handle_dm(msg)
            return

        logging.debug(f"event: message from {msg.author.name} in {msg.channel.name}")

        # look for a started game in the current channel/thread
        game = self.games.get(msg.channel.id)

        # TODO role mention?
        if user in msg.mentions:
            await self.handle_mention(msg, game, user.id == BETA_USER_ID)
            return

        if game is None:
            return

        content = msg.content.strip()

        try:
            guess = float(content)
        except ValueError:
            return

        # hacky way of excluding ridiculous guesses (long decimals, too, now that we have float support)
        if len(content) > MAX_GUESS_SIZE:
            await msg.add_reaction("❌")
            await msg.channel.send(
                f"Too big.",
                reference=msg,
                allowed_mentions=discord.AllowedMentions(users=False, replied_user=True),
            )
            return
        try:
            game.add_guess(Guess(msg.author.id, msg.id, guess))
            reacts = self.number_reacts(guess)
            for r in reacts:
                await msg.add_reaction(r)
        except AlreadyGuessedException as e:
            if e.previous_guess.author_id == msg.author.id:
                await msg.reply("That's the same as your current guess.")
            else:
                await msg.add_reaction("❌")
                try:
                    # could fetch the user by e.previous_guess.author_id instead...
                    prev_msg = await msg.channel.fetch_message(e.previous_guess.message_id)
                    prev_author = f" by `@{prev_msg.author.display_name}`"
                except discord.NotFound:
                    prev_author = ""
                await msg.channel.send(
                    f"Sorry, {e.previous_guess.guessed_number} has already been guessed{prev_author}.",
                    reference=msg,
                    allowed_mentions=discord.AllowedMentions(users=False, replied_user=True),
                )
        except TooManyGuessesException:
            await msg.add_reaction("❌")
            await msg.channel.send(
                f"Sorry, this game only supports {game.per_user_limit} guesses per person.",
                reference=msg,
                allowed_mentions=discord.AllowedMentions(users=False, replied_user=True),
            )
        except AlreadyDoneException:
            logging.error(
                f"{msg.author.name} Tried to add a guess for a game that was already completed in "
                f"{msg.guild}/{msg.channel}"
            )

    @staticmethod
    def number_reacts(num: float) -> List[str]:
        res = ["✅"]
        if num % 100 == 69:
            res.append("😏")
        if num == 420:
            res.append("🌿")
        if num == 737:
            res.append("💣")
        if num in [737, 747, 767, 777, 787]:
            res.append("✈️")
        if num == 1337:
            res.append("📟")
        return res

    @staticmethod
    async def on_ready(user: discord.User, guilds: List[discord.Guild]):
        logging.info(f"{user} has connected to Discord!")
        for guild in guilds:
            perms: discord.Permissions = guild.me.guild_permissions
            logging.info(f"\t- {guild.name} (id: {guild.id}) (perms:{perms.value:b})")
